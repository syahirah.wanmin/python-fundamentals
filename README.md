# Python fundamentals

What we learn here:

1. Understand how Python works and how to use it for digital applications.
2. Learn to use Numpy Matplotlib, Pandas, Seaborn for Data analysis.
3. Learn to use machine learning models from Sklearn and how to ensemble and stack to become better model



Refreshing Python programming language
Python language fundamentals, including basic syntax, data types, lists, dictionaries, sets, tuples.
https://colab.research.google.com/drive/1u-UqpvbsdH4CyncvFRyYua5xUgwWBWit

Introduction to linear algebra library, Numpy, and vector programming
What is numpy and why linear algebra is very crucial for data science.
https://colab.research.google.com/drive/12vgr9TBbfDSfdQPdg5guheNOJze-QGgP

Introduction to dataframe library, Pandas
What is pandas and it offers data structures and operations for manipulating numerical tables.
https://colab.research.google.com/drive/13P2UCRqwsYDj1-PTDhi0vGbet8UUddp3

Pandas Reduction
How to mean, min, median, sum, max, kurtosis, value counts, simple plotting, boolean indexing and sort values
https://colab.research.google.com/drive/19WGK-gmU79QY4-WMkkT3ogIct_JThlsj

Pandas fast indexing and selecting data
We will focus on how to slice, dice, and generally get and set subsets of pandas objects.
https://colab.research.google.com/drive/1eOAL9CREvzVCdc-K6vUWiT5nYWRIdDDl

Pandas grouping for aggregation, filtration and transformation
What is aggregation and how to aggregate using Pandas.
https://colab.research.google.com/drive/106K-gX3bjHsVvFSZr22mPhb9MfjrICj-

Disclaimer :
1. The initial codes are inspired by Husein Zolkepli, edited by me to suit with my own cases and scenario used for this code. 
2. Please make sure to credit all these codes before sharing or using it. 
3. Use Google account for Google Colab, https://colab.research.google.com/ 
4. or Github account incase Google Colab is too slow.

